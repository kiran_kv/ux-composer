import Typography from '@mui/material/Typography';
const GHeader = ({header}) => {
    return(
        <Typography
                sx={{
                    textTransform: 'uppercase',
                    fontWeight: 'large',
                }}
                gutterBottom
                variant="h4"
                align={'left'}
                >
                {header}
        </Typography>
    )
}
export default GHeader;