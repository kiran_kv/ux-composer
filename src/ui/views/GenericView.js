import GCarousalNew from '../../ui/GCarousalNew';
import GHeader from '../../ui/GHeader';
import GProductGroup from '../../ui/GProductGroup';
const GenericView = ({block,viewDetails}) => {

    const getLayout = (blockL,uiLayout) => {

    }

    return(
        <>
        {
            block.layout.map((singleCol) => {
                if('record_header'===singleCol){
                    return <GHeader header={viewDetails.record.record_header}/>
                }else if('productImages'===singleCol){
                    return <GProductGroup images={viewDetails.record.productImages}/>
                }else if('@group' ===singleCol){

                }else if('&relatedProducts'===singleCol){
                    return <GCarousalNew/>
                }
                //return getLayout(singleCol,block)
        })
      }
        </>
    )
}
export default GenericView;