import  global from "../../utils/global.js";
import GenericLayoutView from './GenericLayoutView';
const GColumnView = ({block,viewDetails}) => {
    let divCount = 12 / block.layout.length;

    let fromGeneric = false;
    let tabData = undefined;
    if (line.length > 0) {
            fromGeneric = true;
            (tabData = {
                heading: block.type,
                index: lIndex + self.props.recordId
                    }),
                      (line +=
                        " hidden " +
                        block.type +
                        "" +
                        lIndex +
                        self.props.recordId);
    }

    let record = viewDetails.record ? Object.assign({}, viewDetails.record.value): undefined;
    let schema = viewDetails.schema;
    let parentLayout = this.props.parentLayout? this.props.parentLayout : undefined; // need to check, it is coming from parent
    try {
        parentLayout = schema["@operations"]["read"][viewDetails.viewName].UILayout.type; //need to check, this returning array
    } catch (err) {
        console.log("error from reading parentLayout from details object",err)
    }
    var displayName = this.props.displayName ? this.props.displayName : "yes"; // need to check
    
    const getLayout = (singleCol, fullLayout, tabData, fromGeneric) => {
            return <GenericLayoutView
                    key={global.guid()}
                    singleCol={singleCol}
                    parentLayout={parentLayout}
                    fullLayout={fullLayout}
                    displayName={fullLayout.showDisplayNames == undefined ||fullLayout.showDisplayNames == ""? displayName: fullLayout.showDisplayNames}
                    updateRecord={this.updateRecord} // need to check
                    schemaDoc={schema}
                    data={record}
                    viewName={viewDetails.viewName}
                    methods={viewDetails.record ? viewDetails.record.methods : []} // need to check, no methods from record object
                    relatedSchemas={viewDetails.record && viewDetails.record.relatedSchemas? viewDetails.record.relatedSchemas: []} // need to check, no related schemas in record object
                    
                    
                    />
    }

    return(
        <>
            {block.layout.map(function(singleCol, index) {
                        if (!Array.isArray(singleCol)) {
                          singleCol = [singleCol];
                        }
                        return (
                          <div
                            className={
                              divType +
                              (index + 1 == block.layout.length
                                ? " no-padding"
                                : "")
                            }
                            key={global.guid()}
                          >
                            {singleCol.map(function(element) {
                              return self.getLayout(
                                element,
                                block,
                                tabData,
                                fromGeneric
                              );
                            })}
                          </div>
                        );
                      })}
        </>
    )

}
export default GColumnView;
