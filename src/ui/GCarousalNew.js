import React from 'react';
import Slider from 'react-slick';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { colors } from '@mui/material';
import {api} from '../scripts/api'

import useSWR from 'swr';

const fetcher = (url,payload) => fetch(url,payload).then((res) => res.json())

const Spaces = () => {

    const theme = useTheme();
    const isMd = useMediaQuery(theme.breakpoints.up('md'), {
      defaultMatches: true,
    });
  
    const sliderOpts = {
      dots: false,
      arrows: true,
      infinite: true,
      slidesToShow: isMd ? 3 : 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    };

    let relatedRecordsPayload = {"recordId":"","relationName":"hasProduct",
    "rootSchema":"Product","relationRefSchema":"Product",
    "relationView":"CarouselGallery","skip":0,"org":"public",
    "sortBy":"displayPriority","sortOrder":"descend"};

    let url = "https://localhost:9500/generic/getRelatedRecords?data="+JSON.stringify(relatedRecordsPayload)
    console.log('Relatedrecords  URL :::: ', url);
    const { data, error } = useSWR(url, fetcher);
    if(error){
      console.log("error ");
    }
    if (!data) return <div>loading...</div>
    console.log("relatedRecords data", data);

  return (
    <Box>
      
      <Box maxWidth={{ xs: 420, sm: 620, md: 1 }} margin={'0 auto'}>
        <Slider {...sliderOpts}>
        {data && data.records.map((item, i) => (
            <Box key={i} padding={{ xs: 1, md: 2, lg: 3 }}>
              <Box
                display={'block'}
                width={1}
                height={1}
                sx={{
                  textDecoration: 'none',
                  transition: 'all .2s ease-in-out',
                  '&:hover': {
                    transform: `translateY(-${theme.spacing(1 / 2)})`,
                  },
                }}
              >
                <Box
                  component={Card}
                  width={1}
                  height={1}
                  display={'flex'}
                  flexDirection={'column'}
                  sx={{ backgroundImage: 'none' }}
                >
                  <CardMedia
                    title={item.value.record_header}
                    image={item.value.productImages[0].produtImages[0].url}
                    sx={{
                      position: 'relative',
                      height: { xs: 240, sm: 340, md: 280 },
                      overflow: 'hidden',
                    }}
                  >
                  </CardMedia>
                  <CardContent>
                    <Typography
                      variant={'h6'}
                      gutterBottom
                      align={'left'}
                      sx={{ fontWeight: 700 }}
                    >
                      {item.value.record_header}
                    </Typography>
                  </CardContent>
                  <Box flexGrow={1} />
                  {/* <CardActions sx={{ justifyContent: 'flex-end' }}>
                    <Button>Learn more</Button>
                  </CardActions> */}
                </Box>
              </Box>
            </Box>
          ))}
        </Slider>
      </Box>
    </Box>
  );
};

export default Spaces;
// export async function getServerSideProps(context) {
//   // Call an external API endpoint to get posts.
//   // You can use any data fetching library
//   let relatedRecordsPayload = {"recordId":"","relationName":"hasProduct",
//     "rootSchema":"Product","relationRefSchema":"Product",
//     "relationView":"CarouselGallery","skip":0,"org":"public",
//     "sortBy":"displayPriority","sortOrder":"descend"};

//   const res = await api.getRelatedRecordView(relatedRecordsPayload);
//   console.log("related record res is",res);
//   // By returning { props: { posts } }, the Blog component
//   // will receive `posts` as a prop at build time
//   return {
//     props: {
//       data:res
//     },
//   }
// }
