import React from 'react';
import Slider from 'react-slick';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';

import useSWR from 'swr';

const fetcher = (url,payload) => fetch(url,payload).then((res) => res.json())

const GCarouselGallery = () => {

    const theme = useTheme();
    const isXs = useMediaQuery(theme.breakpoints.up('xs'), {
      defaultMatches: true,
    });
    const isSm = useMediaQuery(theme.breakpoints.up('sm'), {
      defaultMatches: true,
    });
    const isMd = useMediaQuery(theme.breakpoints.up('md'), {
      defaultMatches: true,
    });
    const isLg = useMediaQuery(theme.breakpoints.up('lg'), {
      defaultMatches: true,
    });
  
    let slidesToShow = 2;
    if (isXs) {
      slidesToShow = 2;
    }
    if (isSm) {
      slidesToShow = 3;
    }
    if (isMd) {
      slidesToShow = 4;
    }
    if (isLg) {
      slidesToShow = 5;
    }
  
    const sliderOpts = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow,
      slidesToScroll: 1,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 2000,
    };

    let relatedRecordsPayload = {"recordId":"","relationName":"hasProduct",
    "rootSchema":"Product","relationRefSchema":"Product",
    "relationView":"CarouselGallery","skip":0,"org":"public",
    "sortBy":"displayPriority","sortOrder":"descend"};

    let url = "https://localhost:9500/generic/getRelatedRecords?data="+JSON.stringify(relatedRecordsPayload)
    //console.log('Manufacturer Name URL :::: ', url);
    const { data, error } = useSWR(url, fetcher);
    if (!data) return <div>loading...</div>
    console.log("relatedRecords data", data);

    return(
        <>
            <Box style={{padding:'60px',backgroundColor:'blue'}}>
       <Slider {...sliderOpts}>
        {data.records.map((item, i) => (

            <Box maxWidth={120} key={i} marginX={3}>
            <Box
            component="img"
            height={1}
            width={1}
            src={item.value.productImages[0].produtImages[0].url}
            alt="..."
            />
            </Box>
          
        ))}
      </Slider>
    </Box>
        </>
    )
}
export default GCarouselGallery;