import Box from '@mui/material/Box';
const GProductGroup = ({images}) => {
    return(<>
        {
            images.map(prodImages=>(
                prodImages.produtImages.map(image=>(
                    <Box sx={{width: 300,
                        height: 300}} component="img" src={image.url}/>
                ))
            ))
        }
        </>
    )
}
export default GProductGroup;