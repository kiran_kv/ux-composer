import {useEffect,useState} from 'react';
import { useRouter } from 'next/router';
import APPConfig from '../../../appConfig';
import Head from 'next/head'
//import fetch from 'unfetch'
import useSWR from 'swr';
import DetailsView from '../../views/DetailsView';
import {api} from '../../scripts/api'
//const fetcher = (url,payload) => fetch(url,payload).then((res) => res.json())
const fetcher = (...args) => fetch(...args).then((res) => res.json())

const ItemDetails =  () => {
  console.log('from ItemDetails page');
  const router = useRouter()
  
  const { org,itemrecordslug } = router.query

  let recordViewPayload ={schema:"Product",dependentSchema:"WallLamp",recordId:itemrecordslug,org:org}
  let url ='https://localhost:9500/generic/getSchemaRecordForView?data='+recordViewPayload;
  let newUrl = APPConfig.baseUrl+'/api/records?data='+JSON.stringify(recordViewPayload);

  // const swrResponse = useSWR(newUrl, fetcher);
  // console.log("Data from useSwr ItemDetails", swrResponse);

  const {data,error} = useSWR(newUrl, fetcher);
  if(error){
    console.log("err is ",error)
  }
  if(data){
    console.log("data is ---- ",data)
  } 
  if (!data) return <div>loading...</div>

  return (<>
              <Head>
                <title>{data && data.record && data.record.record_header}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
              </Head>
              <DetailsView viewDetails={data} org={org}/>
          </>
  )
}

export default ItemDetails
// export async function getServerSideProps(context) {
//   // Call an external API endpoint to get posts.
//   // You can use any data fetching library
//   console.log("Nowwwwww ", context.params);
//   //let d ={"schema":"","dependentSchema":localStorage.getItem('dependentSchema'),"recordId":context.query.itemrecordslug,"org":context.params.org}
//   let d ={"schema":"Product","dependentSchema":"WallLamp","recordId":context.query.itemrecordslug,"org":context.params.org}
//   let url ="https://localhost:9500/generic/getSchemaRecordForView?data="+d
//   const res = await api.getRecordView(d);
//   console.log("now res is",res);
//   //const posts = await res.json()
// let posts;
//   // By returning { props: { posts } }, the Blog component
//   // will receive `posts` as a prop at build time
//   return {
//     props: {
//       viewDetails:res
//     },
//   }
// }
