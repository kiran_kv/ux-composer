import {useEffect,useState,React} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import Link from 'next/link';

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import {api} from '../scripts/api';
import FiltersView from './FiltersViewNew';
import GalleryView from './GalleryView';
import TableView from './TableView';
import CollapsibleTableView from './CollapsibleTableView';
import CardView from './CardView';
import AccordionView from './AccordianView';
import useSWR from 'swr';
import APPConfig from '../../appConfig';

const fetcher = (url,payload) => fetch(url,payload).then((res) => res.json())

const SummaryDetails = ({schemaRecords,summaryDetailsLink,schemaRecordsCount,payload,applicableFilters}) => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });
  
  const [page, setPage] = useState('');
  const [schemaRecordsArr, setSchemaRecordsArr] = useState(schemaRecords);

  const handleChange = async (event) => {
    setPage(event.target.value);
    console.log('skip number is :: ', (event.target.value-1)*24);
    payload.skip = (event.target.value-1)*24;
    console.log("new payload is ::: ",payload);
    // let schemaRecordsNew =  await api.getSchemaRecords(payload);
    // console.log("Filtered schema records :: ", schemaRecordsNew);
    // setSchemaRecordsArr(schemaRecordsNew);

    const schemaRecordsNew = await fetch(APPConfig.baseUrl+"/api/records", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(payload)
    });
    const recordsNew = await schemaRecordsNew.json()
    setSchemaRecordsArr(recordsNew);
  };

  const handleFilters = async (filter) => {
     console.log(" Payload ::: ",payload);
      console.log('Filters from child to parent',filter);
      let exisitingFilter = payload.filters;
      payload.filters = {...exisitingFilter, [filter.filterKey]:filter.filterValue}
      console.log("Extra filters added ::: ",payload);

      // let schemaRecordsNew =  await api.getSchemaRecords(payload);
      // console.log("Filtered schema records :: ", schemaRecordsNew);
      // setSchemaRecordsArr(schemaRecordsNew);

      const schemaRecordsNew = await fetch(APPConfig.baseUrl+"/api/records", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(payload)
      });
      const recordsNew = await schemaRecordsNew.json()
      setSchemaRecordsArr(recordsNew);
  }
  console.log("type of schemaRecordsCount", typeof(schemaRecordsCount));
  console.log("Number of pages ", Math.ceil(schemaRecordsCount.total/24));

  // const showTableView = (schemaRecordsData) => {
  //   let tabData = schemaRecordsData.records.map((record,i)=> {
  //     return {name:record.value.name,url:record.value.productImages[0].produtImages[0].url}
  //     })
  //   return <TableView tabData={tabData} headers=""/>
  // }

  return (
    <Box style={{padding:'30px'}}>
      <Box marginBottom={10}>
        <Typography
          sx={{
            textTransform: 'uppercase',
            fontWeight: 'medium',
          }}
          gutterBottom
          color={'secondary'}
          align={'center'}
        >
          {summaryDetailsLink}
        </Typography>
        <Typography
          variant="h4"
          align={'center'}
          data-aos={'fade-up'}
          gutterBottom
          sx={{
            fontWeight: 700,
          }}
        >
          Better way to find a property new
        </Typography>
        <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Select Page</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={page}
              label="Select Page"
              onChange={handleChange}
            >
              {
                [...Array(Math.ceil(schemaRecordsCount.total/24))].map((_, i) => (<MenuItem value={i+1}>{i+1}</MenuItem>))
              }
            </Select>
        </FormControl>
        <FiltersView applicableFilters={applicableFilters} handleFilters={handleFilters}/>
        
        { schemaRecords.schema['@views'][0].viewName==='summary' && 
          <GalleryView schemaRecords={schemaRecordsArr}/> 
        }
        { schemaRecords.schema['@views'][0].viewName==='summary8' && 
          <TableView schemaRecords={schemaRecordsArr}/> 
        }
        { schemaRecords.schema['@views'][0].viewName==='summary4' && 
          <CollapsibleTableView schemaRecords={schemaRecordsArr}/>
        }
        { schemaRecords.schema['@views'][0].viewName==='summary7' &&
          <CardView schemaRecords={schemaRecordsArr}/>
        }
        {schemaRecords.schema['@views'][0].viewName==='summary7' &&
          <AccordionView schemaRecords={schemaRecordsArr}/>
        }
      </Box>

      
    </Box>
  );
};

export default SummaryDetails;
