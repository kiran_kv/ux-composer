import {useEffect} from 'react';
import GTable from '../ui/GTable';

const TableVeiw = ({schemaRecords}) => {
    let data = schemaRecords.records.map((record,i)=> {
        return {name:record.value.name,url:record.value.productImages[0].produtImages[0].url}
    });
    console.log("Table data ",data);
    console.log("Table Headers", Object.keys(data[0]));
    let headers = Object.keys(data[0]);

    return(
        <>
            <GTable headers={headers} data={data}/>
        </>
    )
}
export default TableVeiw;