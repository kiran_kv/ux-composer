import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import useSWR from 'swr';
import GCarouselGallery from '../ui/GCarouselGallery';
import GCarousalNew from '../ui/GCarousalNew';
import GHeader from '../ui/GHeader';
import GProductGroup from '../ui/GProductGroup';
import GenericView from '../ui/views/GenericView'


const fetcher = (url,payload) => fetch(url,payload).then((res) => res.json())

const DetailsView = ({viewDetails,org}) => {
    
    const getDetails  = viewDetails.schema['@operations'].read.getDetail;

    console.log("Read from details page !!!", getDetails);
    let manufaturerNamePayload = {org:org,schema:"Manufacturer",recordId:viewDetails.record.Manufacturer}
    // let url = "https://localhost:9500/generic/getSchemaRecordForView?data="+JSON.stringify(manufaturerNamePayload);
    // console.log("getSchemaRecordForView url ::: ",url)
    // const { data, error } = useSWR(url, fetcher);
    // if (!data) return <div>loading...</div>
    // console.log("Data from useSwr", data);

    const showRelatedProducts = (uiView) => {
        return (
            <GCarousalNew/>
        )
    }
    const showRecordHeader = (uiView) => {
        return(
            <GHeader header={viewDetails.record.record_header}/>
        )
    }
    const showProductImages = (uiView) => {
        return(
            <GProductGroup images={viewDetails.record.productImages}/>
        )
    }
    return( <>
        <Box>
            <Divider/>
            <Box sx={{width:'50%'}}>
                <Box sx={{display:'flex', justifyContent:'space-between'}}>
                    <Box>
                        <Typography sx={{ fontWeight: 'bold' }} variant="h5">Manufacturer</Typography>
                        {/* <Typography>{data.record.name}</Typography> */}
                    </Box>
                    <Box>
                        <Typography sx={{ fontWeight: 'bold' }}>Style</Typography>
                        <Typography>{viewDetails.record.style}</Typography>
                    </Box>
                    <Box>
                        <Typography sx={{ fontWeight: 'bold' }}>Shape</Typography>
                        <Typography>{viewDetails.record.shape}</Typography>
                    </Box>
                </Box>
            </Box>
            <Divider/>
            <Box>
                <Box sx={{width:'50%'}}>

                </Box>
                <Box sx={{width:'50%'}}>
                    <Typography sx={{ fontWeight: 'bold' }}>Specifications</Typography>
                    <Typography variant='p'>{viewDetails.record.specifications}</Typography>
                </Box>
            </Box>
            {
                getDetails.UILayout.map(uiView =>{

                    if(['generic','gallery','summaryTable'].includes(uiView.type)){
                            return <GenericView block={uiView} viewDetails={viewDetails}/>
                    }

                    // if(uiView.layout[0]==='record_header'){
                    //     return showRecordHeader(uiView);
                    // }
                    // if(uiView.layout[0]==='productImages'){
                    //     return showProductImages(uiView);
                    // }
                    // if(uiView.layout[0]==='&relatedProducts'){
                    //     return showRelatedProducts(uiView);
                    // }
                })
            }
        </Box>
        </>
    )

}
export default DetailsView;