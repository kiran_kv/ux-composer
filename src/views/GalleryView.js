import {React} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Link from 'next/link';


const GalleryView = ({schemaRecords}) => {

    return(
        <Grid container spacing={1}>
        {schemaRecords && schemaRecords.records && schemaRecords.records.map((record, i) => (
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            key={i}
            data-aos={'fade-up'}
            data-aos-delay={i * 50}
            data-aos-offset={50}
            data-aos-duration={300}
          >
            <Box display={'block'} width={1} height={1}>
              <Box
                component={Card}
                width={1}
                height={1}
                display={'flex'}
                flexDirection={'column'}
              >
                
           {  
                <Link underline="none" href={ {pathname:'/details/'+record.value.recordId, query: { org: record.value.org }}}>
                                            {/* <CardMedia
                                            title={record.value.productImages[0].produtImages[0].caption}
                                            image={record.value.productImages[0].produtImages[0].url}
                                                sx={{
                                                padding:'5px',
                                                position: 'relative',
                                                height: { xs: 140, sm: 240, md: 280 },
                                                overflow: 'hidden',
                                                }}
                                            >
                                            </CardMedia> */}
                                             <CardMedia
          component="img"
          height="140"
          width="140"
          image={record.value.productImages[0].produtImages[0].url}
          alt="green iguana"
        />
                  </Link>
            }
                <CardContent sx={{backgroundColor:'grey'}}>
                  <Typography
                    variant={'h6'}
                    align={'left'}
                    sx={{ fontWeight: 700 }}
                  >
                    {record.value.name}
                  </Typography>
                </CardContent>
              </Box>
            </Box>
          </Grid>
        ))}
      </Grid>
    )

}
export default GalleryView;