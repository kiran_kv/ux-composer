import GAccordion from '../ui/GAccordion'

const AccordionView = ({schemaRecords}) => {

  let data = schemaRecords.records.map((record,i)=> {
    return {name:record.value.name,url:record.value.productImages[0].produtImages[0].url}
  });
  
  return (
    <GAccordion data={data}/>
  );
}
export default AccordionView
